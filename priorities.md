I am wholesale copying this from [Kenny Johnston's]() excellent [priorities.md file](https://gitlab.com/kencjohnston/kencjohnston/-/blob/master/Priorities.md) which is actually used as the example for a priorities.md in the GitLab Manager Onboarding template. I will update this and make it my own as time goes on, but I'm using this as a boilerplate to start.

## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis. 

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not completed

## 2021-12-13
1. Continue through onboarding activities
    1. In particular, make good progress on my [General Gitlab "Becoming a Manager" Issue](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1425)
    2. And complete, at least, steps 2 and 3 of [my Becoming a Support Manager Onboarding Pathway](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1425)
2. Begin handoff from my team's previous manager with myself and schedule 1:1:1s with all.
3. Shadow support activities to get an initial understanding of support flow, etc.
4. Get at least five MRs created out of the backlog of suggestions/fixes I have been noting while onboarding.
